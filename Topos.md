feat: grid

- make 3x3 grid items
- appendchild every grid item to grid
- make array & push all items

feat:topo

- make node img with topo.png
- subfeat: eventclick
  - when click on topo png sent call feat score
  - change src to topo-angry.png
- subfeat: move
  - if is move is true -> add class .animation
- subfeat: reset
  - if is reset -> remove .animation & src: topo.png

feat: score push
get #score. turn string to number
add +10 to number
number to string & #score.textContent = new string

feat: controlador

- set interval with:
  - set inteval with the time of .animated
  - if topo is in grid -> remove
  - get random grid item from array
  - reset topo (? si se añade de nuevo quizas no hace falta resetearlo)
  - move (? si se añade de nuevo quizas resetea la animación)
  - push topo node to grid item

feat: timer

- set Date (?)
- countdown
- if countdown is 00:00 -> remove set interval
