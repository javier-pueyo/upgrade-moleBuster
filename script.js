

const gridItems = [];

const timeleftStart = 20;
const seconds$$ = document.getElementById("seconds");
seconds$$.textContent = timeleftStart;
const score$$ = document.getElementById('score');
const startButton$$ = document.getElementById('start');

const grid$$ = document.querySelector('.grid');

for (let i = 0; i < 9; i++) {

  const gridItem$$ = document.createElement('div');
  gridItem$$.className = 'grid__item';
  grid$$.append(gridItem$$);
  gridItems.push(gridItem$$);

}


const scorePush = (oneClick) => {

  if (oneClick) {

    let scoreNumber$$ = score$$.textContent;
    scoreNumber$$ = Number(scoreNumber$$) + 10;
    score$$.textContent = scoreNumber$$;

  }

  return false;

}


const mole$$ = document.createElement('img');
mole$$.setAttribute('src', './assets/img/topo.png');
mole$$.setAttribute('width', '100px');

const interactionWithMole = (mole$$, oneClick) => {

  mole$$.addEventListener('click', () => {
    mole$$.setAttribute('src', './assets/img/topo-angry.png');
    oneClick = scorePush(oneClick);
  });

  document.addEventListener('dragstart', function(e) {
    if (e.target.tagName == 'IMG') {
      e.preventDefault();
    }
  });

}


const randomIntFromInterval = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) + min)
}


const mechanics = () => {

  const moleClone$$ = mole$$.cloneNode(true);
  let oneClick = true;
  oneClick = interactionWithMole(moleClone$$, oneClick);
  gridItems[randomIntFromInterval(0,8)].appendChild(moleClone$$);

}


const startGame = () => {

  if (startButton$$.textContent == 'Start') {

    const moveMoleInterval = window.setInterval(mechanics, 1300);
    let timeleft = timeleftStart;
    seconds$$.textContent = timeleft;
    
    const countdownInterval = setInterval(() => {
      timeleft--;
      seconds$$.textContent = ("0" + timeleft).slice(-2);
      if(timeleft <= 0) {
        clearInterval(countdownInterval);
        clearInterval(moveMoleInterval);
    }},1000);

  return {moveMoleInterval,countdownInterval}

  }
}

const restartGame = (intervals) => {
  startButton$$.textContent = 'Start';
  clearInterval(intervals.countdownInterval);
  clearInterval(intervals.moveMoleInterval);
  score$$.textContent = '00';
  seconds$$.textContent = timeleftStart;
}

startButton$$.addEventListener('click', function() {

  const intervals = startGame();
  if(intervals != null) itervalsCopy = intervals;

  if (startButton$$.textContent  == 'Start') {
    startButton$$.textContent = 'Reset';

  } else {
    restartGame(itervalsCopy);
  }

});






